# Finance Tracker

This is a project repository for the KAJ semestral work - Finance Tracker.

## Description

Finance Tracker is a simple SPA that allows user to track their Expenses and Incomes.

The user will input date, name of the input, its type and amount. 
There are several basic expense types to choose from, such as Food, Entertainment, Housing etc. Additional expenses for which no type exists can be declared as Other.

The application uses localStorage to save the expenses. (I would ideally like to continue this project over summer and create a more complex version of it including a proper backend and frontend modifications).

Pie chart is visualized using js and svg.


## Used Technologies

- JavaScript
- html
- CSS
- frameworks (jQuery for toggling classes)

