class Expense {
    constructor(date, name, type, cost) {
        this.date = date;
        this.name = name;
        this.type = type;
        this.cost = cost;
    }
}

/**
 * Adds expense into a localStorage
 */
function addExpense(date, name, type, cost){
    const expense = new Expense(date, name, type, cost);
    const expenseList = JSON.parse(localStorage.getItem('expenses')) || [];
    expenseList.push(expense);
    localStorage.setItem('expenses', JSON.stringify(expenseList));
    updateExpenseList();

    const audio = new Audio('cash.mp3');
    audio.play();
}

/**
 * Deletes an expense form localStorage
 * @param index
 */
function deleteExpense(index) {
    const expenseList = JSON.parse(localStorage.getItem('expenses')) || [];
    expenseList.splice(index, 1);
    localStorage.setItem('expenses', JSON.stringify(expenseList));
    updateExpenseList();

    const audio = new Audio('swoosh.mp3');
    audio.play();
}

/**
 * Updates displayed expense list
 */
function updateExpenseList() {
    const expenseList = document.getElementById('expense-list');
    expenseList.innerHTML = '';

    const filterSelect = document.getElementById('filter-select');
    const filterType = filterSelect.value;

    const yearSelect = document.getElementById('year-select1');
    const yearSelectValue = parseInt(yearSelect.value);

    const monthSelect = document.getElementById('month-select1');
    const monthSelectValue = parseInt(monthSelect.value);

    const expenses = JSON.parse(localStorage.getItem('expenses')) || [];
    const filteredExpenses = expenses.filter(expense => {
        if (filterType !== 'all' && expense.type !== filterType) {
            return false;
        }

        if (yearSelectValue !== 0 && monthSelectValue !== 0) {
            const expenseDate = new Date(expense.date);
            const expenseYear = expenseDate.getFullYear();
            const expenseMonth = expenseDate.getMonth() + 1;
            return expenseYear === yearSelectValue && expenseMonth === monthSelectValue;
        }

        return true;
    });

    //renders each expense as a list item; colours expense costs amounts as red, incomes as green
    filteredExpenses.forEach((expense, index) => {
        const row = document.createElement('div');
        const expenseAmountClass = expense.type === 'Income' ? 'income' : 'expense';

        row.className = 'row';

        row.innerHTML = `
            <div class="cell"> <button class="delete-btn" onclick="deleteExpense(${index})">Delete</button> </div>
            <div class="cell">${expense.date}</div> <div class="cell">(${expense.type}) </div>  <div class="cell"> ${expense.name} </div> <div class="cell"> <span class="${expenseAmountClass}">${expense.cost},-</span></div></div> </div>
        `;
        expenseList.appendChild(row);
    });

    //gets expense totals by type
    const expenseTotals = {};
    filteredExpenses.forEach(expense => {
        if (expenseTotals[expense.type]) {
            expenseTotals[expense.type] += expense.cost;
        } else {
            expenseTotals[expense.type] = expense.cost;
        }
    });

    const totalCost = Object.values(expenseTotals).reduce((acc, curr) => acc + curr, 0);

    generatePieChart(expenseTotals, totalCost);
}


//form submission
const expenseForm = document.getElementById('expense-form');
expenseForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const nameInput = document.getElementById('name-input');
    const amountInput = document.getElementById('amount-input');
    const typeSelect = document.getElementById('type-select');
    const dateInput = document.getElementById('date-input');

    const name = sanitizeInput(nameInput.value.trim());
    const amount = parseFloat(amountInput.value);
    const type = sanitizeInput(typeSelect.value);
    const date = sanitizeInput(dateInput.value);

    if (name !== '' && !isNaN(amount)) {
        addExpense(date, name, type, amount);
        nameInput.value = '';
        amountInput.value = '';
        typeSelect.value = 'food';
        dateInput.value = '';
    }
});

/**
 * Helper function to protect inputs from breaking the page
 */
function sanitizeInput(input) {
    // Replace '<' and '>' characters with their HTML entity equivalents
    return input.replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

//filtering expenses by type
const filterSelect = document.getElementById('filter-select');
filterSelect.addEventListener('change', function () {
    updateExpenseList();
});

const yearSelect = document.getElementById('year-select1');
yearSelect.addEventListener('change',function () {
    updateExpenseList();
})

const monthSelect = document.getElementById('month-select1');
monthSelect.addEventListener('change',function () {
    updateExpenseList();
})

/**
 * Generates pie chart
 */
function generatePieChart(expenseTotals, totalCost) {
    const chartSVG = document.getElementById('expense-chart');

    const centerX = 200;
    const centerY = 200;
    const radius = 150;
    const tooltipOffset = 20;

    let startAngle = 0;

    //color palette for expense types
    const colorPalette = {
        Food: '#38B000',
        Entertainment: '#70E000',
        Transport: '#9EF01A',
        Housing: '#CCFF33',
        Savings: '#008000',
        Income: '#006400',
        Other: '#004B23'
    };

    //alternative in blue
    /*
    const colorPalette = {
        Food: '#90E0EF',
        Entertainment: '#48CAE4',
        Transport: '#00B4D8',
        Housing: '#0096C7',
        Savings: '#0077B6',
        Income: '#023E8A',
        Other: '#03045E'
    };

     */

    //clears the chart slices
    while (chartSVG.firstChild) {
        chartSVG.firstChild.remove();
    }

    // draws slice for each expense
    Object.keys(expenseTotals).forEach(type => {
        const expenseAmount = expenseTotals[type];
        const percentage = (expenseAmount / totalCost) * 100;

        const endAngle = startAngle + (Math.PI * 2 * (expenseAmount / totalCost));

        const x1 = centerX + Math.sin(startAngle) * radius;
        const y1 = centerY - Math.cos(startAngle) * radius;

        const x2 = centerX + Math.sin(endAngle) * radius;
        const y2 = centerY - Math.cos(endAngle) * radius;

        const largeArcFlag = percentage > 50 ? 1 : 0;

        //path element for the slice
        const path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path.setAttribute('d', `M${centerX},${centerY} L${x1},${y1} A${radius},${radius} 0 ${largeArcFlag},1 ${x2},${y2} Z`);
        path.setAttribute('fill', colorPalette[type]);

        //tooltip to show the cost, type and percentage on hover
        const tooltip = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        tooltip.setAttribute('class', 'tooltip');
        tooltip.setAttribute('x', centerX);
        tooltip.setAttribute('y', centerY - radius - tooltipOffset);
        tooltip.textContent = `${type} (${expenseAmount} CZK,- , ${percentage.toFixed(2)}%)`;
        tooltip.style.opacity = '0';

        path.addEventListener('mouseenter', () => {
            path.style.opacity = '0.7';
            tooltip.style.opacity = '1';
        });
        path.addEventListener('mouseleave', () => {
            path.style.opacity = '1';
            tooltip.style.opacity = '0';
        });

        chartSVG.appendChild(path);
        chartSVG.appendChild(tooltip);

        //transition effects
        path.style.transition = 'fill 1s ease-in-out';
        path.style.fill = 'transparent';
        setTimeout(() => {
            path.style.fill = colorPalette[type];
        }, 10);

        startAngle = endAngle;
    });
}

/**
 * Generates options for Year select for pie chart
 */
function generateYearOptions() {
    const yearSelect = document.getElementById('year-select');
    const yearSelect1 = document.getElementById('year-select1');
    const currentYear = new Date().getFullYear();
    const earliestYear = 2000;
    for (let year = currentYear; year >= earliestYear; year--) {
        const option = document.createElement('option');
        const option1 = document.createElement('option');


        option.value = year;
        option.textContent = year;

        option1.value = year;
        option1.textContent = year;

        yearSelect.appendChild(option);
        yearSelect1.appendChild(option1);
    }
}

/**
 * Generates options for Month select for pie chart
 */
function generateMonthOptions() {
    const monthSelect = document.getElementById('month-select');
    const monthSelect1 = document.getElementById('month-select1');
    const monthNames = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    for (let month = 1; month <= 12; month++) {
        const option = document.createElement('option');
        const option1 = document.createElement('option');


        option.value = month;
        option.textContent = monthNames[month - 1];

        option1.value = month;
        option1.textContent = monthNames[month - 1];

        monthSelect.appendChild(option);
        monthSelect1.appendChild(option1);
    }
}

/**
 * Generates pie chart for selected year and month
 */
function generatePieChartForMonth(year, month) {
    const expenses = JSON.parse(localStorage.getItem('expenses')) || [];
    const filteredExpenses = expenses.filter(expense => {
        const expenseDate = new Date(expense.date);
        return expenseDate.getFullYear() === year && expenseDate.getMonth() === month - 1;
    });

    const expenseTotals = {};
    filteredExpenses.forEach(expense => {
        if (expenseTotals[expense.type]) {
            expenseTotals[expense.type] += expense.cost;
        } else {
            expenseTotals[expense.type] = expense.cost;
        }
    });

    const totalCost = Object.values(expenseTotals).reduce((acc, curr) => acc + curr, 0);

    generatePieChart(expenseTotals, totalCost);
}

document.addEventListener('DOMContentLoaded', function() {
    generateYearOptions();
    generateMonthOptions();

    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    yearSelect.value = currentYear;
    monthSelect.value = currentMonth;

    const generateChartButton = document.getElementById('generateChartButton');
    generateChartButton.addEventListener('click', function() {
        const yearSelect = document.getElementById('year-select');
        const monthSelect = document.getElementById('month-select');
        const year = parseInt(yearSelect.value);
        const month = parseInt(monthSelect.value);

        generatePieChartForMonth(year, month);
    });

});


//initial page load
updateExpenseList();
